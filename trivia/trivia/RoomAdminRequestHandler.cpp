#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory& factory, LoggedUser user, unsigned int roomID) : RoomMemberRequestHandler(factory, user, roomID)
{
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo request)
{
	return request.id == LEAVE_ROOM || request.id == START_GAME || request.id == GET_ROOM_STATE;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
	// admin specific actions
	if (requestInfo.id == START_GAME)
		return this->startGame();
	// member actions
	return RoomMemberRequestHandler::handleRequest(requestInfo);
}

RequestResult RoomAdminRequestHandler::leaveRoom()
{
	// close room
	this->m_roomManager.deleteRoom(this->m_roomID);
	// send response
	Buffer responseBuffer = JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse{ 1 });
	return RequestResult{ responseBuffer, this->m_handlerFactory.createMenuRequestHandler(this->m_user) };	// back to menu
}

IRequestHandler* RoomAdminRequestHandler::copyHandler()
{
	return this->m_handlerFactory.createRoomAdminRequestHandler(this->m_user, this->m_roomID);
}

RequestResult RoomAdminRequestHandler::startGame()
{
	// change state to active
	this->m_handlerFactory.getGameManager().createGame(m_roomManager.getRoom(m_roomID));
	this->m_roomManager.getRoom(this->m_roomID).setActive();
	//gets out of room and deletes it if needed
	m_roomManager.getRoom(this->m_roomID).removeUser(this->m_user);
	if (m_roomManager.getRoom(this->m_roomID).getAllUsers().size() == 0)
		m_roomManager.deleteRoom(this->m_roomID);
	// return response
	Buffer responseBuffer = JsonResponsePacketSerializer::serializeResponse(StartGameResponse{ 1 });
	return RequestResult{ responseBuffer, this->m_handlerFactory.createGameRequestHandler(m_roomID, m_user) };	
}
