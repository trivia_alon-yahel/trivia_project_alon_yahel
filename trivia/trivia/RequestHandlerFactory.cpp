#include "RequestHandlerFactory.h"
#include "RoomAdminRequestHandler.h"

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    return new LoginRequestHandler(*this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
    return new MenuRequestHandler(*this, user);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(roomID roomID, LoggedUser user)
{
    return new GameRequestHandler(*this, roomID, user);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, roomID room)
{
    return new RoomAdminRequestHandler(*this, user, room);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, roomID room)
{
    return new RoomMemberRequestHandler(*this, user, room);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
    return this->m_loginManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return this->m_statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
    return this->m_roomManager;
}

QuestionManager& RequestHandlerFactory::getQuestionManager()
{
    return this->m_questionManager;
}


GameManager& RequestHandlerFactory::getGameManager()
{
    return this->m_gameManager;
}
